struct array_element
{
	int element;
	struct array_element * next;
	struct array_element * prev;
};

struct intarray
{
	struct array_element * first;
	struct array_element * last;
	int lowbound;
	int highbound;
	int buffer;
};

typedef struct intarray * intarray;

struct intarray * new_array();
struct intarray * create_array(int lb);
struct intarray * copy_array(struct intarray * parray);
struct intarray * fill_copy_array(int lb, int cnt, int elem);
int low_array(struct intarray * parray);
int high_array(struct intarray * parray);
int size_array(struct intarray * parray);
int fetch_array(struct intarray * parray, int i);
void store_array(struct intarray * parray, int i, int elem);
int addh_array(struct intarray * parray, int elem);
int addl_array(struct intarray * parray, int elem);
int remh_array(struct intarray * parray);
int reml_array(struct intarray * parray);
void delete_array(struct intarray * parray);