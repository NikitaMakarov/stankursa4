#include <stdio.h>
#include "intarray.h"

void print_array(struct intarray * parray)
{
	int i;
	for (i = low_array(parray); i <= high_array(parray); i++)
		printf("%d\t", fetch_array(parray, i));

	printf("\n\n");

}

int main()
{
	int i;
	intarray c = new_array();

	int l = low_array(c);
	int h = high_array(c);

	// 1) add 4 elements to low bound
	for (i = 0; i < 4; i++)               
		addl_array(c, i);
	print_array(c);


	// 2) add 4 elements to high bound
	for (i = 4; i < 9; i++)               
		addh_array(c, i);
	print_array(c);


	// 3) remove the 2 lowest elements
	for (i = 0; i < 2; i++)               
		reml_array(c);
	print_array(c);


	// 4) remove the 2 highest elements
	for (i = 0; i < 2; i++)               
		remh_array(c);
	print_array(c);

	int mid = (high_array(c) - low_array(c)) / 2 + low_array(c);

	// 5) swap an elements
	for (i = low_array(c); i <= mid; i++) 
	{
		int temp = fetch_array(c, high_array(c) - i);
		store_array(c, high_array(c) - i, fetch_array(c, low_array(c) + i));
		store_array(c, low_array(c) + i, temp);
	}
	print_array(c);


	// 6) clone an array
	intarray cp = copy_array(c);     
	print_array(cp);


	/*       
	 *  7) create an array of			4
	 *	   default value is				3
	 *     low bound is					5
	 */
	intarray c_filled = fill_copy_array(5, 4, 3);
	print_array(c_filled);

	
	delete_array(c);
	delete_array(cp);
	delete_array(c_filled);

	return 0;
}