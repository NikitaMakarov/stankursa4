#include "intarray.h"
#include <stdlib.h>
#include <limits.h>

// +
struct intarray * new_array()
{
	intarray new_array = (intarray)(malloc(sizeof(struct intarray)));

	new_array->highbound = 0;
	new_array->lowbound = 1;
	new_array->first = 0;
	new_array->last = 0;
	new_array->buffer = 0;

	return new_array;
}

// +
struct intarray * create_array(int lb)
{
	intarray new_array = (intarray)(malloc(sizeof(struct intarray)));

	new_array->highbound = lb-1;
	new_array->lowbound = lb;
	new_array->first = 0;
	new_array->last = 0;
	new_array->buffer = 0;

	return new_array;
}

// +
struct intarray * copy_array(struct intarray * parray)
{
	struct intarray * new_instance = create_array(parray->lowbound);

	struct array_element * ptr = parray->first;
	while (ptr != 0)
	{
		addh_array(new_instance, ptr->element);
		ptr = ptr->next;
	}

	return new_instance;
}

// +
struct intarray * fill_copy_array(int lb, int cnt, int elem)
{
	if (cnt < 0)
		return 0;
	struct intarray * new_instance = create_array(lb);
	if (new_instance == 0)
		return 0;

	unsigned int counter = 0;

	while (counter < cnt)
	{
		int result = addh_array(new_instance, elem);
		if (result == 1)
		{
			delete_array(new_instance);
			return 0;
		}
		counter++;
	}
	return new_instance;
}

// +
int low_array(intarray parray)
{
	return parray->lowbound;
}


int high_array(intarray parray)
{
	return parray->highbound;
}

// +
int size_array(intarray parray)
{
	if (parray->last == 0)
		return 0;
	return parray->highbound - parray->lowbound + 1;
}

// +
int fetch_array(intarray parray, int i)
{
	int l = low_array(parray);
	int h = high_array(parray);

	if (i < l || h < i)
		return 0;

	int mid = (h - l) / 2 + l;

	if (i < mid)
	{
		struct array_element * ptr = parray->first;
		int counter = l;
		while (counter != i)
		{
			ptr = ptr->next;
			counter++;
		}
		return ptr->element;
	}
	else
	{
		struct array_element * ptr = parray->last;
		int counter = h;
		while (counter != i)
		{
			ptr = ptr->prev;
			counter--;
		}
		return ptr->element;
	}
}


// +
void store_array(intarray parray, int i, int elem)
{
	int l = low_array(parray);
	int h = high_array(parray);

	if (i < l || h < i)
		return;

	int mid = (h - l) / 2 + l;

	if (i < mid)
	{
		struct array_element * ptr = parray->first;
		int counter = l;
		while (counter != i)
		{
			ptr = ptr->next;
			counter++;
		}
		ptr->element = elem;
	}
	else
	{
		struct array_element * ptr = parray->last;
		int counter = h;
		while (counter != i)
		{
			ptr = ptr->prev;
			counter--;
		}
		ptr->element = elem;
	}
}

// +
int addh_array(intarray parray, int elem)
{
	if (INT_MAX == parray->highbound)
		return 1;
	
	struct array_element * new_instance = (struct array_element *)(malloc(sizeof(struct array_element)));
	if (new_instance == 0)
		return 1;

	new_instance->element = elem;
	new_instance->next = 0;
	new_instance->prev = parray->last;

	if (parray->last == 0)
		parray->first = new_instance;
	else
		parray->last->next = new_instance;
	
	parray->last = new_instance;
	(parray->highbound)++;
	return 0;
}

// +
int addl_array(intarray parray, int elem)
{
	if (INT_MIN == parray->highbound)
		return 1;

	struct array_element * new_instance = (struct array_element *)(malloc(sizeof(struct array_element)));
	if (new_instance == 0)
		return 1;

	new_instance->element = elem;
	new_instance->next = parray->first;
	new_instance->prev = 0;

	if (parray->first == 0)
		parray->last = new_instance;
	else
		parray->first->prev = new_instance;

	parray->first = new_instance;
	(parray->lowbound)--;
	return 0;
}

// +
int remh_array(intarray parray)
{
	// int result = 0;
	if (parray->last != 0)
	{
		struct array_element * temp = parray->last;
		if (parray->last->prev != 0)
		{
			parray->last = parray->last->prev;
			parray->last->next = 0;
			parray->buffer = temp->element;
			free(temp);
		}
		else
		{
			parray->first = 0;
			parray->last = 0;
			parray->buffer = temp->element;
			free(temp);
		}
		(parray->highbound)--;
	}
	return parray->buffer;
}

// +
int reml_array(intarray parray)
{
	if (parray->first != 0)
	{
		struct array_element * temp = parray->first;
		if (parray->first->next != 0)
		{
			parray->first = parray->first->next;
			parray->first->prev = 0;
			parray->buffer = temp->element;
			free(temp);
		}
		else
		{
			parray->first = 0;
			parray->last = 0;
			parray->buffer = temp->element;
			free(temp);
		}
		(parray->lowbound)++;
	}
	return parray->buffer;
}

// +
void delete_array(intarray parray)
{
	while (size_array(parray) != 0)
		remh_array(parray);
	free(parray);
}