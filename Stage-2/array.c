#include "array.h"
#include <stdlib.h>
#include <limits.h>

void * copy(void * source, int tsize)
{
	void * result = malloc(tsize);
	if (result == 0)
		return 0;
	int i;
	for (i = 0; i < tsize; i++)
		*((char *)result + i) = *((char *)source + i);
	return result;
}

// +
array new_array(int elem_size)
{
	array new_array = (array)(malloc(sizeof(struct array)));

	new_array->highbound = 0;
	new_array->lowbound = 1;
	new_array->first = 0;
	new_array->last = 0;
	new_array->buffer = 0;
	new_array->tsize = elem_size;

	return new_array;
}

// +
array create_array(int lb, int elem_size)
{
	array new_array = (array)(malloc(sizeof(struct array)));

	new_array->highbound = lb - 1;
	new_array->lowbound = lb;
	new_array->first = 0;
	new_array->last = 0;
	new_array->buffer = 0;
	new_array->tsize = elem_size;

	return new_array;
}

// +
array copy_array(array parray)
{
	array new_instance = create_array(parray->lowbound, parray->tsize);

	struct array_element * ptr = parray->first;
	while (ptr != 0)
	{
		addh_array(new_instance, ptr->element);
		ptr = ptr->next;
	}

	return new_instance;
}

// +
array fill_copy_array(int lb, int cnt, void * elem, int elem_size)
{
	if (cnt < 0)
		return 0;
	array new_instance = create_array(lb, elem_size);
	if (new_instance == 0)
		return 0;

	unsigned int counter = 0;

	while (counter < cnt)
	{
		void * result = addh_array(new_instance, elem);
		if (result == 0)
		{
			delete_array(new_instance);
			return 0;
		}
		counter++;
	}
	return new_instance;
}

// +
int low_array(array parray)
{
	return parray->lowbound;
}


int high_array(array parray)
{
	return parray->highbound;
}

// +
int size_array(array parray)
{
	if (parray->last == 0)
		return 0;
	return parray->highbound - parray->lowbound + 1;
}

// +
void * fetch_array(array parray, int i)
{
	int l = low_array(parray);
	int h = high_array(parray);

	if (i < l || h < i)
		return 0;

	int mid = (h - l) / 2 + l;

	if (i < mid)
	{
		struct array_element * ptr = parray->first;
		int counter = l;
		while (counter != i)
		{
			ptr = ptr->next;
			counter++;
		}
		return ptr->element;
	}
	else
	{
		struct array_element * ptr = parray->last;
		int counter = h;
		while (counter != i)
		{
			ptr = ptr->prev;
			counter--;
		}
		return ptr->element;
	}
}


// +
void store_array(array parray, int i, void * elem)
{
	int l = low_array(parray);
	int h = high_array(parray);

	if (i < l || h < i)
		return;

	int mid = (h - l) / 2 + l;

	if (i < mid)
	{
		struct array_element * ptr = parray->first;
		int counter = l;
		while (counter != i)
		{
			ptr = ptr->next;
			counter++;
		}
		// free(ptr->element);
		ptr->element = copy(elem, parray->tsize);
	}
	else
	{
		struct array_element * ptr = parray->last;
		int counter = h;
		while (counter != i)
		{
			ptr = ptr->prev;
			counter--;
		}
		// free(ptr->element);
		ptr->element = copy(elem, parray->tsize);
	}
}

// +
void * addh_array(array parray, void * elem)
{
	if (INT_MAX == parray->highbound)
		return 0;

	struct array_element * new_instance = (struct array_element *)(malloc(sizeof(struct array_element)));
	if (new_instance == 0)
		return 0;

	new_instance->element = copy(elem, parray->tsize);
	if (new_instance->element == 0)
	{
		free(new_instance);
		return 0;
	}
	new_instance->next = 0;
	new_instance->prev = parray->last;

	if (parray->last == 0)
		parray->first = new_instance;
	else
		parray->last->next = new_instance;

	parray->last = new_instance;
	(parray->highbound)++;
	return new_instance->element;
}

// +
void * addl_array(array parray, void * elem)
{
	if (INT_MIN == parray->highbound)
		return 0;

	struct array_element * new_instance = (struct array_element *)(malloc(sizeof(struct array_element)));
	if (new_instance == 0)
		return 0;

	new_instance->element = copy(elem, parray->tsize);
	if (new_instance->element == 0)
	{
		free(new_instance);
		return 0;
	}
	new_instance->next = parray->first;
	new_instance->prev = 0;

	if (parray->first == 0)
		parray->last = new_instance;
	else
		parray->first->prev = new_instance;

	parray->first = new_instance;
	(parray->lowbound)--;
	return new_instance->element;
}

// +
void * remh_array(array parray)
{
	// int result = 0;
	if (parray->last != 0)
	{
		struct array_element * temp = parray->last;
		if (parray->last->prev != 0)
		{
			parray->last = parray->last->prev;
			parray->last->next = 0;
			if (parray->buffer != 0)
				free(parray->buffer);
			parray->buffer = temp->element;
			free(temp);
		}
		else
		{
			parray->first = 0;
			parray->last = 0;
			if (parray->buffer != 0)
				free(parray->buffer);
			parray->buffer = temp->element;
			free(temp);
		}
		(parray->highbound)--;
	}
	return parray->buffer;
}

// +
void * reml_array(array parray)
{
	if (parray->first != 0)
	{
		struct array_element * temp = parray->first;
		if (parray->first->next != 0)
		{
			parray->first = parray->first->next;
			parray->first->prev = 0;
			if (parray->buffer != 0)
				free(parray->buffer);
			parray->buffer = temp->element;
			free(temp);
		}
		else
		{
			parray->first = 0;
			parray->last = 0;
			if (parray->buffer != 0)
				free(parray->buffer);
			parray->buffer = temp->element;
			free(temp);
		}
		(parray->lowbound)++;
	}
	return parray->buffer;
}

// +
void delete_array(array parray)
{
	while (size_array(parray) != 0)
		remh_array(parray);
	if (parray->buffer != 0)
		free(parray->buffer);
	free(parray);
}