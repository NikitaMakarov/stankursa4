#include <stdio.h>
#include "array.h"
#include <stdlib.h>
void print_array(array parray)
{
	int i;
	for (i = low_array(parray); i <= high_array(parray); i++)
		printf("%.1f\t", *((double *)fetch_array(parray, i)));

	printf("\n\n");

}

void * getDouble(double d)
{
	double * result = (double *)(malloc(sizeof(double)));
	*result = d;
	return result;
}

int main()
{
	int i;
	array c = new_array(sizeof(double));

	int l = low_array(c);
	int h = high_array(c);

	// 1) add 4 elements to low bound
	for (i = 0; i < 4; i++)
		addl_array(c, getDouble(i));
	print_array(c);

	 l = low_array(c);
	 h = high_array(c);

	// 2) add 4 elements to high bound
	for (i = 4; i < 9; i++)
		addh_array(c, getDouble(i));
	print_array(c);




	// 3) remove the 2 lowest elements
	for (i = 0; i < 2; i++)
		reml_array(c);
	print_array(c);


	// 4) remove the 2 highest elements
	for (i = 0; i < 2; i++)
		remh_array(c);
	print_array(c);

	int mid = (high_array(c) - low_array(c)) / 2 + low_array(c);

	// 5) swap an elements
	int counter = 0;
	for (i = low_array(c); i <= mid; i++)
	{
		int low = i;
		int high = high_array(c) -(i - low_array(c));
		double * low_value = (double *)fetch_array(c, low);
		double * high_value = (double *)fetch_array(c, high);
		
		store_array(c, high, low_value);
		store_array(c, low, high_value);
		counter++;

	}
	print_array(c);


	// 6) clone an array
	array cp = copy_array(c);
	print_array(cp);


	/*
	*  7) create an array of			4
	*	   default value is				3
	*     low bound is					5
	*/
	array c_filled = fill_copy_array(5, 4, getDouble(3), sizeof(double));
	print_array(c_filled);


	delete_array(c);
	delete_array(cp);
	delete_array(c_filled);

	return 0;
}