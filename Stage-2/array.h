struct array_element
{
	void * element;
	struct array_element * next;
	struct array_element * prev;
};

struct array
{
	struct array_element * first;
	struct array_element * last;
	int tsize;
	int lowbound;
	int highbound;
	void * buffer;
};

typedef struct array * array;

struct array * new_array(int elem_size);
struct array * create_array(int lb, int elem_size);
struct array * copy_array(struct array * parray);
struct array * fill_copy_array(int lb, int cnt, void * elem, int elem_size);
int low_array(struct array * parray);
int high_array(struct array * parray);
int size_array(struct array * parray);
void * fetch_array(struct array * parray, int i);
void store_array(struct array * parray, int i, void * elem);
void * addh_array(struct array * parray, void * elem);
void * addl_array(struct array * parray, void * elem);
void * remh_array(struct array * parray);
void * reml_array(struct array * parray);
void delete_array(struct array * parray);