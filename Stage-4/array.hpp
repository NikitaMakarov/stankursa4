#include <stddef.h>
#include <limits.h>

template <class Any> class Array
{
	struct ArrayElement
	{
		Any element;
		ArrayElement * next;
		ArrayElement * prev;

		ArrayElement()
		{
			next = NULL;
			prev = NULL;
		}

		ArrayElement(const Any& _element)
		{
			element = _element;
			next = NULL;
			prev = NULL;
		}

		ArrayElement(const Any& _element, ArrayElement * _prev, ArrayElement * _next)
		{
			element = _element;
			prev = _prev;
			next = _next;
		}
	};

	ArrayElement * first;
	ArrayElement * last;
	int lowbound;
	int highbound;
	ArrayElement * current_element;
	int current_index;
	Any buffer;

public:
	Array();
	Array(int lb);
	Array(Array& parray);
	Array(int lb, int cnt, const Any& _element);
	int Low();
	int High();
	int Size();
	Any& Fetch(int index);
	void Store(int index, Any& _element);
	Any& operator[](int index);
	void AddH(const Any& _element);
	void AddL(const Any& _element);
	Any& RemH();
	Any& RemL();
	Any& operator--(int){ return RemH(); };
	Any& operator--(){ return RemL(); };
	~Array();
};

template <class Any> inline void operator+(const Any& _element, Array<Any>& intarray);
template <class Any> inline void operator+(Array<Any>& intarray, const Any& _element);



template <class Any>
Array<Any>::Array()
{
	first = NULL;
	last = NULL;
	lowbound = 1;
	highbound = 0;
	current_element = NULL;
	current_index = 0;
}


template <class Any>
Array<Any>::Array(int lb)
{
	first = NULL;
	last = NULL;
	lowbound = lb;
	highbound = lb - 1;
	current_element = NULL;
	current_index = 0;
}


template <class Any>
Array<Any>::Array(Array& parray)
{
	first = NULL;
	last = NULL;
	lowbound = parray.lowbound;
	highbound = parray.lowbound - 1;
	current_element = NULL;
	current_index = 0;

	ArrayElement * ptr = parray.first;
	while (ptr != NULL)
	{
		AddH(ptr->element);
		ptr = ptr->next;
	}
}


template <class Any>
Array<Any>::Array(int lb, int cnt, const Any& _element)
{
	first = NULL;
	last = NULL;
	lowbound = lb;
	highbound = lb - 1;
	current_element = NULL;
	current_index = 0;

	int i;
	for (i = 0; i < cnt; i++)
		AddH(_element);
}


template <class Any>
inline int Array<Any>::Low()
{
	return lowbound;
}


template <class Any>
inline int Array<Any>::High()
{
	return highbound;
}


template <class Any>
inline int Array<Any>::Size()
{
	return highbound - lowbound + 1;
}

template <class Any>
Any& Array<Any>::Fetch(int index)
{
	int mid = (highbound - lowbound) / 2 + lowbound;

	if (index < mid)
	{
		ArrayElement * ptr = first;
		int counter = lowbound;
		while (counter != index)
		{
			ptr = ptr->next;
			counter++;
		}
		return ptr->element;
	}
	else
	{
		ArrayElement * ptr = last;
		int counter = highbound;
		while (counter != index)
		{
			ptr = ptr->prev;
			counter--;
		}
		return ptr->element;
	}
}


template <class Any>
void Array<Any>::Store(int index, Any& _element)
{
	if (index < lowbound || highbound < index)
		return;

	int mid = (highbound - lowbound) / 2 + lowbound;

	if (index < mid)
	{
		ArrayElement * ptr = first;
		int counter = lowbound;
		while (counter != index)
		{
			ptr = ptr->next;
			counter++;
		}

		ptr->element = _element;
	}
	else
	{
		ArrayElement * ptr = last;
		int counter = highbound;
		while (counter != index)
		{
			ptr = ptr->prev;
			counter--;
		}
		
		ptr->element = _element;
	}
}


template <class Any>
Any& Array<Any>::operator[](int index)
{
	if (index < lowbound || highbound < index)
		throw;

	int mid = (highbound - lowbound) / 2 + lowbound;

	if (index < mid)
	{
		ArrayElement * ptr = first;
		int counter = lowbound;
		while (counter != index)
		{
			ptr = ptr->next;
			counter++;
		}

		return ptr->element;
	}
	else
	{
		ArrayElement * ptr = last;
		int counter = highbound;
		while (counter != index)
		{
			ptr = ptr->prev;
			counter--;
		}

		return ptr->element;
	}
}


template <class Any>
void Array<Any>::AddH(const Any& _element)
{
	if (INT_MAX == highbound)
		return;

	ArrayElement * new_instance = new ArrayElement(_element, last, NULL);
	if (last != NULL)
		last->next = new_instance;
	last = new_instance;
	if (first == NULL)
		first = new_instance;
	highbound++;
}


template <class Any>
void Array<Any>::AddL(const Any& _element)
{
	if (INT_MIN == lowbound)
		return;

	ArrayElement * new_instance = new ArrayElement(_element, NULL, first);
	if (first != NULL)
		first->prev = new_instance;
	first = new_instance;
	if (last == NULL)
		last = new_instance;
	lowbound--;
}


template <class Any>
Any& Array<Any>::RemH()
{
	// if (last == NULL)
	//	return 0;

	buffer = last->element;

	ArrayElement * temp = NULL;
	if (last->prev != NULL)
	{
		last->prev->next = NULL;
		temp = last->prev;
	}
	else
		first = NULL;

	delete last;
	last = temp;
	highbound--;

	return buffer;
}


template <class Any>
Any& Array<Any>::RemL()
{
	// if (first == NULL)
	//	return;
	buffer = first->element;

	ArrayElement * temp = NULL;
	if (first->next != NULL)
	{
		first->next->prev = NULL;
		temp = first->next;
	}
	else
		last = NULL;

	delete first;
	first = temp;
	lowbound++;

	return buffer;
}

template <class Any>
Array<Any>::~Array()
{
	ArrayElement * ptr = first;
	while (ptr != NULL)
	{
		ArrayElement * temp = ptr->next;
		delete ptr;
		ptr = temp;
	}
}

template <class Any>
inline void operator+(const Any& _element, Array<Any>& intarray)
{
	intarray.AddL(_element);
}


template <class Any>
inline void operator+(Array<Any>& intarray, const Any& _element)
{
	intarray.AddH(_element);
}
