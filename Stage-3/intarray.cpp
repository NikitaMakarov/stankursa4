#include "intarray.hpp"


IntArray::IntArray()
{
	first = NULL;
	last = NULL;
	lowbound = 1;
	highbound = 0;
	current_element = NULL;
	current_index = 0;
	buffer = 0;
}


IntArray::IntArray(int lb)
{
	first = NULL;
	last = NULL;
	lowbound = lb;
	highbound = lb - 1;
	current_element = NULL;
	current_index = 0;
	buffer = 0;
}


IntArray::IntArray(IntArray& parray)
{
	first = NULL;
	last = NULL;
	lowbound = parray.lowbound;
	highbound = parray.lowbound - 1;
	current_element = NULL;
	current_index = 0;
	buffer = 0;

	ArrayElement * ptr = parray.first;
	while (ptr != NULL)
	{
		AddH(ptr->element);
		ptr = ptr->next;
	}
}

IntArray::IntArray(int lb, int cnt, int _element)
{
	first = NULL;
	last = NULL;
	lowbound = lb;
	highbound = lb - 1;
	current_element = NULL;
	current_index = 0;
	buffer = 0;

	int i;
	for (i = 0; i < cnt; i++)
		AddH(_element);
}

int IntArray::Low()
{
	return lowbound;
}


int IntArray::High()
{
	return highbound;
}


int IntArray::Size()
{
	return highbound - lowbound + 1;
}


int IntArray::Fetch(int index)
{
	if (index < lowbound || highbound < index)
		return 0;

	int mid = (highbound - lowbound) / 2 + lowbound;

	if (index < mid)
	{
		ArrayElement * ptr = first;
		int counter = lowbound;
		while (counter != index)
		{
			ptr = ptr->next;
			counter++;
		}
		return ptr->element;
	}
	else
	{
		ArrayElement * ptr = last;
		int counter = highbound;
		while (counter != index)
		{
			ptr = ptr->prev;
			counter--;
		}
		return ptr->element;
	}
}

void IntArray::Store(int index, int _element)
{
	if (index < lowbound || highbound < index)
		return;

	int mid = (highbound - lowbound) / 2 + lowbound;

	if (index < mid)
	{
		ArrayElement * ptr = first;
		int counter = lowbound;
		while (counter != index)
		{
			ptr = ptr->next;
			counter++;
		}
		ptr->element = _element;
	}
	else
	{
		ArrayElement * ptr = last;
		int counter = highbound;
		while (counter != index)
		{
			ptr = ptr->prev;
			counter--;
		}
		ptr->element = _element;
	}
}

int& IntArray::operator[](int index)
{
	if (index < lowbound || highbound < index)
		throw;

	int mid = (highbound - lowbound) / 2 + lowbound;

	if (index < mid)
	{
		ArrayElement * ptr = first;
		int counter = lowbound;
		while (counter != index)
		{
			ptr = ptr->next;
			counter++;
		}

		return ptr->element;
	}
	else
	{
		ArrayElement * ptr = last;
		int counter = highbound;
		while (counter != index)
		{
			ptr = ptr->prev;
			counter--;
		}

		return ptr->element;
	}
}

void IntArray::AddH(int _element)
{
	if (INT_MAX == highbound)
		return;

	ArrayElement * new_instance = new ArrayElement(_element, last, NULL);
	if (last != NULL)
		last->next = new_instance;
	last = new_instance;
	if (first == NULL)
		first = new_instance;
	highbound++;
}


void IntArray::AddL(int _element)
{
	if (INT_MIN == lowbound)
		return;

	ArrayElement * new_instance = new ArrayElement(_element, NULL, first);
	if (first != NULL)
		first->prev = new_instance;
	first = new_instance;
	if (last == NULL)
		last = new_instance;
	lowbound--;
}


int IntArray::RemH()
{
	if (last == NULL)
		return 0;

	buffer = last->element;
	ArrayElement * temp = NULL;
	if (last->prev != NULL)
	{
		last->prev->next = NULL;
		temp = last->prev;
	}
	else
		first = NULL;
		
	delete last;
	last = temp;
	highbound--;

	return buffer;
}


int IntArray::RemL()
{
	if (first == NULL)
		return 0;

	buffer = first->element;
	ArrayElement * temp = NULL;
	if (first->next != NULL)
	{
		first->next->prev = NULL;
		temp = first->next;
	}
	else
		last = NULL;

	delete first;
	first = temp;
	lowbound++;

	return buffer;
}


int IntArray::operator--(int)
{
	return RemH();
}


int IntArray::operator--()
{
	return RemL();
}


IntArray::~IntArray()
{
	while (Size() != 0)
		RemH();
}


/*void operator+(int _element, IntArray& intarray)
{
	intarray.AddL(_element);
}


void operator+(IntArray& intarray, int _element)
{
	intarray.AddH(_element);
}*/