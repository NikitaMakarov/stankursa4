#include <stddef.h>
#include <limits.h>

class IntArray
{
	struct ArrayElement
	{
		int element;
		ArrayElement * next;
		ArrayElement * prev;

		ArrayElement()
		{
			element = 0;
			next = NULL;
			prev = NULL;
		}

		ArrayElement(int _element)
		{
			element = _element;
			next = NULL;
			prev = NULL;
		}

		ArrayElement(int _element, ArrayElement * _prev, ArrayElement * _next)
		{
			element = _element;
			prev = _prev;
			next = _next;
		}
	};
		
	ArrayElement * first;
	ArrayElement * last;
	int lowbound;
	int highbound;
	ArrayElement * current_element;
	int current_index;
	int buffer;
	
public:
	IntArray();
	IntArray(int lb);
	IntArray(IntArray& parray);
	IntArray(int lb, int cnt, int _element);
	int Low();
	int High();
	int Size();
	int Fetch(int index);
	void Store(int index, int _element);
	int& operator[](int index);
	void AddH(int _element);
	void AddL(int _element);
	int RemH();
	int RemL();
	int operator--(int);
	int operator--();
	~IntArray();
};

void operator+(int _element, IntArray& intarray);
void operator+(IntArray& intarray, int _element);

inline void operator+(int _element, IntArray& intarray)
{
	intarray.AddL(_element);
}


inline void operator+(IntArray& intarray, int _element)
{
	intarray.AddH(_element);
}
