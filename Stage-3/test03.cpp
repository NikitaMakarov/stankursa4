#include <iostream>
#include <string>
#include "intarray.hpp"


void print(IntArray &intarray, std::string message)
{
	std::cout << message << ":\t";
	int i;
	for (i = intarray.Low(); i <= intarray.High(); i++)
		std::cout << intarray[i] << "\t";
	std::cout << "\n\n";
}


void assert(int a, int b, std::string message)
{
	std::cout << message << ": " << (a == b ? "OK" : "FAIL") << "\n\n";
}


int main()
{
	IntArray * ptr_array_1 = new IntArray();
	IntArray & test_array_1 = *ptr_array_1;
	assert(test_array_1.Low(), 1, "Lowest element");
	assert(test_array_1.High(), 0, "Highest element");
	print(test_array_1, "Empty array");

	int i;
	for (i = 0; i < 3; i++)
		test_array_1.AddH(i);
	print(test_array_1, "Three elements from zero to two");

	for (; i < 6; i++)
		test_array_1.AddL(i);
	print(test_array_1, "Six elements from five to three and from zero to two");

	assert(test_array_1.Size(), 6, "Array size");

	for (i = test_array_1.Low(); i <= test_array_1.High(); i++)
	{
		test_array_1[i] = test_array_1[i] * -1;
	}
	print(test_array_1, "Negative array");

	for (i = test_array_1.Low(); i <= test_array_1.High(); i++)
	{
		test_array_1.Store(i, test_array_1.Fetch(i) * -1);
	}
	print(test_array_1, "Positive array");

	IntArray * ptr_array_2 = new IntArray(test_array_1);
	print(*ptr_array_2, "Array copy");
	delete ptr_array_2;

	IntArray * ptr_array_3 = new IntArray(3, 4, 5);
	print(*ptr_array_3, "Filled array");
	delete ptr_array_3;

	test_array_1.RemL();
	print(test_array_1, "Remove the lowest element");

	test_array_1.RemH();
	print(test_array_1, "Remove the highest element");

	--test_array_1;
	print(test_array_1, "Remove the lowest element");

	test_array_1--;
	print(test_array_1, "Remove the highest element");

	test_array_1 + 6;
	print(test_array_1, "Adding the new element to top");

	7 + test_array_1;
	print(test_array_1, "Adding the new element to bottom");

	delete ptr_array_1;

	return 0;
}